from PIL import Image
import numpy as np
from osgeo import gdal, osr
import gc
import h3
import utm
import pandas as pd
from tqdm import tqdm

def read_geotiff(filename):
    ds = gdal.Open(filename)
    arr = ds.ReadAsArray()
    return arr, ds

def write_geotiff(filename, arr, in_ds):
    if arr.dtype == np.float32:
        arr_type = gdal.GDT_Float32
    elif arr.dtype == np.uint16:
        arr_type = gdal.GDT_UInt16
    elif arr.dtype == np.uint8:
        arr_type = gdal.GDT_Byte
    else:
        arr_type = gdal.GDT_Int32

    driver = gdal.GetDriverByName("GTiff")
    out_ds = driver.Create(filename, arr.shape[1], arr.shape[0], 1, arr_type)
    out_ds.SetProjection(in_ds.GetProjection())
    out_ds.SetGeoTransform(in_ds.GetGeoTransform())
    band = out_ds.GetRasterBand(1)
    band.WriteArray(arr)
    band.FlushCache()
    band.ComputeStatistics(False)

def get_lat_lon(geotransform, x, y):
    x_min = geotransform[0]
    x_size = geotransform[1]
    y_min = geotransform[3]
    y_size = geotransform[5]
    px = y * x_size + x_min
    py = x * y_size + y_min
    lat, lon = utm.to_latlon(easting=px, northing=py, zone_number=37, zone_letter='N')
    return lat, lon

def get_hexagon(geotransform, x, y):
    lat, lon = get_lat_lon(geotransform, x, y)
    return h3.geo_to_h3(lat=lat, lng=lon, resolution=10)

def add_2d_array(df, basename, array):
    print(array.shape)
    for i in range(array.shape[0]):
        df[basename + '_' + str(i+1)] = array[i, :]

map_admin, ds = read_geotiff('data/KRA_ADMIN_100m.tif') #uint8

geotransform = ds.GetGeoTransform()

map_admin = map_admin.astype(np.bool8)
map_usage, _ = read_geotiff('data/KRA_LANDCOVER_100m.tif') #uint8
map_usage[map_usage == 1] = 0
for val in [3, 5, 6, 10, 11]:
    map_usage[map_usage == val] = 1
map_usage[map_usage != 1] = 0
map_usage = map_usage.astype(np.bool8)

map_water, _ = read_geotiff('data/KRA_WATER_SEASONALYTY_100m.tif')
map_water[map_water > 0] = 1
map_water = map_water.astype(np.bool8)
#map_vineyards, _ = read_geotiff('data/KRA_VINEYARDS_100m.tif') #uint8
#map_vineyards -= 1
#map_vineyards = abs(map_vineyards)
#map_vineyards = map_vineyards.astype(np.bool8)

map_roi = np.logical_and(map_admin, map_usage, map_water)
#map_roi = map_roi.astype(np.uint8)

write_geotiff('roi.tif', map_roi.astype(np.uint8) * 255, ds)

del map_admin
del map_usage
del map_water
gc.collect()

coords = np.nonzero(map_roi)
num_rows = coords[0].shape[0]
#coords = (coords[0][:5], coords[1][:5])
hexagons = []



df = pd.DataFrame()

rains, _ = read_geotiff('data/KRA_PREC_100m.tif')
rains = rains[:, map_roi]
add_2d_array(df, 'rains', rains)
del rains

height, _ = read_geotiff('data/KRA_RELIEF_HEIGHT_100m.tif')
height = height[map_roi]
df['heights'] = height
del height

texture, _ = read_geotiff('data/KRA_SOILTEXTURE_100m.tif')
texture = texture[map_roi]
df['texture'] = texture
del texture

wineyards, _ = read_geotiff('data/KRA_VINEYARDS_WITH_NUMBERS_100m.tif')
wineyards = wineyards[map_roi]
df['vineyards'] = wineyards
del wineyards

sunny_days, _ = read_geotiff('data/KRA_SUNNY_DAYS_APR_OCT_100m.tif')
sunny_days = sunny_days[map_roi]
df['sunny_days'] = sunny_days
del sunny_days

tavg, _ = read_geotiff('data/KRA_TAVG_100m.tif')
tavg = tavg.mean(axis=0)
tavg = tavg[map_roi]
df['tavg'] = tavg

tmin, _ = read_geotiff('data/KRA_TMIN_100m.tif')
tmin = tmin.min(axis=0)
tmin = tmin[map_roi]
df['tmin'] = tmin
#winter_tmin = np.concatenate((tmin[:3, ...], tmin[-3:, ...])).min(axis=0)
#summer_tmin = tmin[5:10, ...].min(axis=0)
#del tmin
#winter_tmin = winter_tmin[map_roi]
#summer_tmin = summer_tmin[map_roi]
#df['winter_tmin'] = winter_tmin
#df['summer_tmin'] = summer_tmin

tmax, _ = read_geotiff('data/KRA_TMAX_100m.tif')
tmax = tmax.max(axis=0)
tmax = tmax[map_roi]
df['tmax'] = tmax

#winter_tmax = np.concatenate((tmax[:3, ...], tmax[-3:, ...])).max(axis=0)
#summer_tmax = tmax[5:10, ...].max(axis=0)
#winter_tmax = winter_tmax[map_roi]
#summer_tmax = summer_tmax[map_roi]
#del tmax
#df['winter_tmax'] = winter_tmin
#df['summer_tmax'] = summer_tmin

for x, y in tqdm(zip(coords[0], coords[1]), total=coords[0].shape[0]):
    hexagons.append(get_hexagon(geotransform, x, y))

hexagons = np.array(hexagons)
df['hexagon'] = hexagons

df.to_parquet('df.parquet')